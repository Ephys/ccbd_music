'use strict';

var api = (function($) {
	var internals = {
		URL_BASE: '//localhost:8081/rest',

		getLoginToken: function() {
			return sessionStorage.getItem('jwt');
		}
	};

	return {
		redirect: function(path) {
			window.location = path + '.html';
		},

		isLogged: function() {
			return internals.getLoginToken() !== null;
		},

		logout: function() {
			sessionStorage.removeItem('jwt');
		},

		setLoginToken: function(token) {
			return sessionStorage.setItem('jwt', token);
		},

		callApi: function(method, path, body) {
			var self = this;
			var url = path.charAt(0) === '/' ? internals.URL_BASE + path : path;
			return $.ajax({
				url: url,
				type: method,
				dataType: 'json',
				data: body || '',
				accepts: {'json': 'application/json'},
				beforeSend: function(xhr) {
					if (self.isLogged()) {
						xhr.setRequestHeader('Authorization', 'Bearer ' + internals.getLoginToken());
					}
				}
			}).fail(function(xhr, error, g) {
				console.warn('request failure', xhr, error, g);
			});
		},

		bindForm: function(form, overwrite) {
			if (overwrite === void 0) {
				overwrite = {};
			} else
				if (typeof overwrite !== 'object') {
					throw new Error('overwrite must be an object');
				}

			var api = this;
			form.addEventListener('submit', function(e) {
				e.preventDefault();

				if (this.hasAttribute('submitting')) {
					return false;
				}

				var inputs = form.querySelectorAll('button[type=submit],input[type=submit]');
				for (var i = 0; i < inputs.length; i++) {
					inputs[i].disabled = true;
				}

				this.setAttribute('submitting', 'submitting');
				var self = this;
				var done = function() {
					self.removeAttribute('submitting');

					for (var i = 0; i < inputs.length; i++) {
						inputs[i].disabled = false;
					}
				};

				if (typeof overwrite.onSubmit === 'function') {
					return overwrite.onSubmit(this, done);
				}

				var method = overwrite.method || form.method;
				var action = overwrite.action || form.getAttribute('action');
				var params = overwrite.body || $(form).serializeArray();

				var call = api.callApi(method, action, params).always(done);
				if (typeof overwrite.dataHandler === 'function') {
					call.done(overwrite.dataHandler);
				}

				if (typeof overwrite.errorHandler === 'function') {
					call.fail(overwrite.errorHandler);
				}

				return false;
			});
		}
	};
})(jQuery);