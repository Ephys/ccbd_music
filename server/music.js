'use strict';
/**
 * =============================
 *
 * Main application.
 *
 * =============================
 */

const config = require('./app/modules/config');
const logger = require('./app/modules/logger');

function startDatabases(cb) {
	let loadedDependencies = 0;

	// TODO: promisify
	const mongo = require('./app/modules/database');
	mongo.start(function(err) {
		if (err) {
			return cb(err);
		} else if (++loadedDependencies === 2) {
			cb();
		}
	});

	const redis = require('./app/modules/redis');
	redis.start(function(err) {
		if (err) {
			return cb(err);
		} else if (++loadedDependencies === 2) {
			cb();
		}
	});
}

function startWebServer() {
	const server = require('./app/modules/server');
	server.start();
}

process.chdir(__dirname);
config.load(function(e) {
	if (e) {
		logger.error(e);
		return;
	}

	startDatabases(function(error) {
		if (error) {
			logger.error(e);
			return;
		}

		startWebServer();
	})
});