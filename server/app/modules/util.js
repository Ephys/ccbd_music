'use strict';

module.exports = {
	ensureBodyParameters: function(request, parameterNameList) {
		for (let parameterName of parameterNameList) {
			if (!request.body[parameterName]) {
				request.res.status(400).json({ error: 'Missing body parameter ' + parameterName });
				return false;
			}
		}

		return true;
	}
};