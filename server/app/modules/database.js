'use strict';

const MongoClient = require('mongodb').MongoClient;
const config = require('./config');
const logger = require('./logger');

const url = config.mongo;
let connection = null;

module.exports = {
    start: function(cb) {
        logger.info('[Mongo] Connecting.');
        MongoClient.connect(url, function(err, db) {
            if (err) return cb(err);
            
            logger.info('[Mongo] Connection established.');
            
            connection = db;
            cb(null);
        });
    },
    stop: function(cb) {
        if (connection == null) {
            cb(new Error('DB not connected'));
            return;
        }
        
        connection.close();

        if (cb) cb(null);
    },
    get connection() {
        return connection;
    }
};