'use strict';

const Redis = require('redis');
const logger = require('../modules/logger');
const config = require('../modules/config');

const url = config.redis;
let connection = null;

module.exports = {
	start: function(cb) {
		logger.info('[Redis] Connecting.');

		connection = Redis.createClient(url);

		function errorListener(error) {
			// avoid calling the cb again when reconnecting
			connection.removeListener('connect', readyListener);
			connection.removeListener('error', errorListener);

			cb(error);
		}

		function readyListener() {
			logger.info('[Redis] Ready.');

			// avoid calling the cb again when reconnecting
			connection.removeListener('connect', readyListener);
			connection.removeListener('error', errorListener);

			if (cb) {
				cb(null);
			}
		}

		connection.on('connect', readyListener);
		connection.on('error', errorListener);
		connection.on('reconnecting', function() {
			logger.info('[Redis] Reconnecting');
		});
	},
	get connection() {
		return connection;
	}
};