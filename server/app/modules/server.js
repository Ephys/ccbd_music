'use strict';

/**
 * =============================
 *
 * API Express server
 * Set the route listening on, start/stop the server...
 *
 * =============================
 *
 * Attributes : /
 *
 * Methods :
 *		- start([callback])
 *		- stop([callback])
 *
 * Events : /
 *
 * =============================
 */

/**
 * Load modules
 */

// Built-in
var express = require('express');
var bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

// Custom
var logger = require('./logger');
var config = require('./config');

// Routes

/**
 * Variables
 */
// Server
var app = express();
var server;

/**
 * Configure application:
 *		- parse json bodies
 */
var _configureServer = function () {
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));

	app.use(function(request, response, next) {
		logger.info('[Server] Received request for ' + request.method + ' ' + request.path);

		response.header('Access-Control-Allow-Origin', '*');
		response.header('Access-Control-Allow-Headers', 'Authorization');

		if (request.method === 'OPTIONS') {
			response.sendStatus(200);
			return;
		}

		next();
	});
};

/**
 * Configure application routes
 */
var _configureRoutes = function () {
	require('../routes/account.js');
	require('../routes/music.js');
};

/**
 * Start the API Server
 *
 * @param callback function called when the web server is listening
 */
var start = function (callback) {
	_configureServer();
	_configureRoutes();

	server = app.listen(process.env.PORT, config.server.host, function () {
		logger.info('[Server] Web server listening on ' + config.server.host + ':' + process.env.PORT);
		if (callback) callback();
	});
};

/**
 * Stop the API Server
 *
 * @param callback function called when the web server is no more listening
 */
var stop = function (callback) {
	if (server && typeof server.close === 'function') {
		server.close();
		logger.warn('[Server] Web server no more listening on ' + config.server.host + ':' + process.env.PORT);
		if (callback) callback();
	} else {
		logger.warn('[Server] Cannot stop web server listening on ' + config.server.host + ':' + process.env.PORT);
		if (callback) callback();
	}
};

var auth = function(request, response, next) {
	//Authorization: Bearer xx
	let authHeader = request.header('Authorization');
	if (authHeader === void 0) {
		response.status(401).send('Authorization header required');
		return;
	}

	let tmp = authHeader.split(' ');
	let authType = tmp[0];
	let authData = tmp[1];
	//let [authType, authData] = authHeader.split(' ');
	
	if (authType !== 'Bearer') {
		response.status(401).send('Authorization type Bearer JWT');
		return;
	}

	jwt.verify(authData, config.jwt_private_key, function(err, decoded) {
		if (err) {
			response.status(401).send('Invalid JWToken');
			return;
		}

		// TODO: get user.
		request.user = decoded;

		next();
	});
};

/**
 * Exports
 */

// Methods
exports.start = start;
exports.stop = stop;

exports.registerRoute = function(method, path, handler) {
	logger.info('[Server] Registering route ' + method + ' ' + path);
	return app[method.toLowerCase()](path, handler);
};

exports.registerAuthRoute = function(method, path, handler) {
	app[method.toLowerCase()](path, auth);
	return exports.registerRoute(method, path, handler);
};