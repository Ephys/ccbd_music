'use strict';

const server = require('../modules/server');
const config = require('../modules/config');
const userDao = require('../dao/userDao');
const util = require('../modules/util');
const logger = require('../modules/logger');

const jwt = require('jsonwebtoken');

/**
 * Login route
 */
server.registerRoute('POST', '/rest/login', function(request, response) {
	if (!util.ensureBodyParameters(request, ['username', 'password'])) {
		return;
	}

	userDao.getByUsername(request.body.username).then(function(user) {
		if (user === null) {
			response.status(401).json({ error: 'invalid username' });
			return;
		}

		// TODO hash.
		if (user.password !== request.body.password) {
			response.status(401).json({ error: 'invalid password' });
			return;
		}

		let token = jwt.sign({username: user.username}, config.jwt_private_key);
		response.json({ token });
	}).catch(function(error) {
		console.log(error);

		response.sendStatus(500);
	});
});

server.registerRoute('POST', '/rest/account', function(request, response) {
	if (!util.ensureBodyParameters(request, ['username', 'password', 'firstName', 'lastName'])) {
		return;
	}

	userDao.getByUsername(request.body.username).then(function(user) {
		if (user !== null) {
			response.status(400).json({ error: 'username already in use' });
			return;
		}

		userDao.add(request.body.username, request.body.password, request.body.firstName, request.body.lastName).then(function() {
			let token = jwt.sign({username: request.body.username}, config.jwt_private_key);
			response.status(200).json({ token });
		}).catch(function(error) {
			logger.error(error);
			response.sendStatus(500);
		});
	});
});