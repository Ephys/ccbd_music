'use strict';

const server = require('../modules/server');
const musicDao = require('../dao/musicDao');
const util = require('../modules/util');

server.registerAuthRoute('POST', '/rest/music', function(request, response) {
	if (!util.ensureBodyParameters(request, ['author', 'title', 'genre'])) {
		return false;
	}

	musicDao.add(request.body.title, request.body.author, request.body.genre);
	response.status(200).json({});
});

server.registerRoute('GET', '/rest/music', function(request, response) {
	musicDao.get().then(function(dataArray) {
		response.status(200).json(dataArray);
	});
});