'use strict';

const config = require('../modules/config');
const dao = require('../modules/database').connection.collection(config.collections.music);

module.exports = {
	add: function(title, author, genre) {
		return dao.insertOne({
			title,
			author,
			genre
		});
	},

	get: function() {
		return dao.find().toArray();
	}
};