'use strict';

const redis = require('../modules/redis').connection;

module.exports = {
	add: function(username, password, firstName, lastName) {
		return new Promise(function(resolve, reject) {
			redis.hmset(username, {
				password,
				firstName,
				lastName
			}, function(err, reply) {
				if (err) {
					return reject(err);
				}

				resolve(reply);
			});
		});
	},

	getByUsername: function(username) {
		return new Promise(function(resolve, reject) {
			redis.hmget(username, ['password', 'firstName', 'lastName'], function(err, reply) {
				if (err) {
					return reject(err);
				}

				if (reply[0] === null) {
					return resolve(null);
				}

				resolve({
					username: username,
					password: reply[0],
					firstName: reply[1],
					lastname: reply[2]
				});
			});
		});
	}
};